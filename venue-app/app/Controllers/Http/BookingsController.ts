import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import BookFormValidator from 'App/Validators/BookFormValidator'
import Field from 'App/Models/Field'
import Booking from 'App/Models/Booking'
import User from 'App/Models/User'
import Venue from 'App/Models/Venue'
import Database from '@ioc:Adonis/Lucid/Database'

export default class BookingsController {
  public async index ({}: HttpContextContract) {
  }
    /**
     * @swagger
     * /api/v1/venues/{venue_id}/bookings:
     *  post:
     *      security:
     *          - bearerAuth: []
     *      tags:
     *          - Venues
     *      summary: Book a Field (From Venue's Field List)
     *      description: Book a field
     *      parameters:
     *        - in: path
     *          name: id
     *          description: id venue 
     *          required: true
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www-form-urlencoded:
     *                  schema:
     *                      $ref: '#definitions/Booking'
     *              application/json:
     *                  schema:
     *                      $ref: '#definitions/Booking'
     *      responses:
     *          201:
     *              description: booking success
     *          422:
     *              description: request invalid
     * 
     */
  public async store ({request, response, params, auth}: HttpContextContract) {
    const venue = await Venue.findByOrFail('id', params.venue_id)
    const user = auth.user!

    const payload = await request.validate(BookFormValidator)

    const booking = new Booking()
    booking.title = payload.title
    booking.PlayDateStart = payload.play_date_start
    booking.PlayDateEnd = payload.play_date_end
    booking.fieldId = request.input('field_id')

    const field = await Field.findByOrFail('id', booking.fieldId)
    await booking.related('field').associate(field)
    await user.related('myBookings').save(booking)

    return response.created({status: 'success get bookings!', data: booking})
  }

  public async show ({params, response}: HttpContextContract) {
    const booking = await Booking.query().where('id', params.id).preload('bookers', (playersQuery)=>{playersQuery.select(['id', 'name', 'email'])}).withCount('players').firstOrFail()
    return response.ok({status: 'success!', data: booking})
  }

  public async update ({}: HttpContextContract) {
  }


  public async destroy ({}: HttpContextContract) {
  }

  public async join ({response, auth, params}:HttpContextContract){
    const booking = await Booking.findOrFail(params.id)
    let user = auth.user!
    const checkJoin = await Database.from('schedules').where('booking_id', params.id).where('user_id', user.id).firstOrFail()
    if (!checkJoin){
      await booking.related('players').attach([user.id])
    }
    else{
      await booking.related('players').detach([user.id])
    }
    return response.ok({message: 'success!'})
  }

  public async schedules ({auth, response}:HttpContextContract){
    let user = auth.user!
    const schedule = await User.query().where('name', user.name).preload('myBookings').firstOrFail()
    response.ok({data: schedule})
    }
}
