import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import VenueFormValidator from 'App/Validators/VenueFormValidator'
import Venue from 'App/Models/Venue'

export default class VenuesController {

    public async index({response, request}:HttpContextContract){
        if (request.qs().name){
            let name = request.qs().name
            let venueFiltered = await Venue.findBy('name', name)
            return response.status(200).json({message: 'success', data: venueFiltered})
        }
        let venues = await Venue.all()
        return response.status(200).json({message: 'success', data: venues})
    }
    /**
     * @swagger
     * /api/v1/venues:
     *  post:
     *      security:
     *          - bearerAuth: []
     *      tags:
     *          - Venues
     *      summary: Create New Venue
     *      description: Register new venue
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www-form-urlencoded:
     *                  schema:
     *                      $ref: '#definitions/Venue'
     *              application/json:
     *                  schema:
     *                      $ref: '#definitions/Venue'
     *      responses:
     *          201:
     *              description: venue added
     *          422:
     *              description: request invalid
     * 
     */
    public async store({request, response}:HttpContextContract){
        
        try {
            await request.validate(VenueFormValidator)
            let newVenue = new Venue();
            newVenue.name = request.input('name'),
            newVenue.address = request.input('address'),
            newVenue.phone = request.input('phone')
            await newVenue.save()
            response.created({messages: 'created!', newId: newVenue})
        } catch (error) {
            response.unprocessableEntity({errors: error.messages})
        }
       
    }

    public async show({response, params}:HttpContextContract){
        let venue = await Venue.query().where('id', params.id).preload('fields').firstOrFail()
        return response.ok({message: 'success', data: venue})
    }

    public async update({request, response, params}:HttpContextContract){
        let id = params.id
        let venueUpdate = await Venue.findOrFail(id)
        venueUpdate.name = request.input('name')
        venueUpdate.address = request.input('address')
        venueUpdate.phone = request.input('phone')
        venueUpdate.save()
        return response.ok({message: 'updated!'})
    }
    
    public async destroy({response, params}:HttpContextContract){
        let venue = await Venue.findOrFail(params.id)
        await venue.delete()
        return response.ok({message: 'deleted!'})
    }
}
