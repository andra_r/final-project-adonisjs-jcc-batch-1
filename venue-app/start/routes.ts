/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from '@ioc:Adonis/Core/Route'

Route.get('/', async () => {
  return { hello: 'world' }
})

// Route.get('/venues', 'VenuesController.index').as('venue.index')
// Route.post('/venues', 'VenuesController.store').as('venue.store')
// Route.get('/venues/:id', 'VenuesController.show').as('venue.show')
// Route.put('/venues/:id', 'VenuesController.update').as('venue.update')
// Route.delete('/venues/:id', 'VenuesController.destroy').as('venue.destroy')
Route.group(()=>{
  Route.group(()=>{
    Route.group(()=>{
      Route.get('/venues', 'VenuesController.index').as('venues.index')
      Route.post('/venues', 'VenuesController.store').as('venues.store')
      Route.get('/venues/:id', 'VenuesController.show').as('venues.show')
      Route.put('/venues', 'VenuesController.update').as('venues.update')
    }).middleware('owner')
    Route.group(()=>{
      Route.post('/venues/:venue_id/bookings', 'BookingsController.store').as('booking.store')
      Route.get('/bookings/', 'BookingsController.index').as('booking.index')
      Route.get('/bookings/:id', 'BookingsController.show').as('booking.show')
      Route.put('/bookings/:id/join', 'BookingsController.join').as('booking.join')
      Route.get('/schedules', 'BookingsController.schedules').as('booking.schedule')
    }).middleware('user')

    // Route.resource('venues', 'VenuesController').apiOnly()
    // Route.resource('venues.fields', 'FieldsController').apiOnly()
    // Route.resource('fields.bookings', 'BookingsController').apiOnly()
    
  }).middleware(['auth', 'verify'])
  
  Route.post('/register', 'AuthController.register').as('auth.register')
  Route.post('/login', 'AuthController.login').as('auth.login')
  Route.post('/otp-confirmation', 'AuthController.otpVerify').as('auth.verify')
}).prefix('/api/v1')



