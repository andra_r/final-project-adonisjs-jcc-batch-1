export enum Type {
    FUTSAL = 'futsal',
    MINI_SOCCER = 'mini soccer',
    BASKETBALL = 'basketball'
}