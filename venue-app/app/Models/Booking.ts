import { DateTime } from 'luxon'
import { BaseModel, BelongsTo, belongsTo, column, ManyToMany, manyToMany } from '@ioc:Adonis/Lucid/Orm'
import Field from 'App/Models/Field'
import User from 'App/Models/User'
/**
 * @swagger
 * definitions:
 *  Booking:
 *    type: object
 *    properties:
 *      title:
 *        type: string
 *      PlayDateStart:
 *        type: dateTime
 *      PlayDateEnd:
 *        type: dateTime
 *      fieldId:
 *        type: integer
 *    required:
 *        -title
 *        -PlayDateStart
 *        -PlayDateEnd
 *        -fieldId
 */
export default class Booking extends BaseModel {
  public serializeExtras = true
  @column({ isPrimary: true })
  public id: number

  @column()
  public title: string

  @column.dateTime()
  public PlayDateStart: DateTime

  @column.dateTime()
  public PlayDateEnd: DateTime

  @column()
  public userId: number

  @column()
  public fieldId: number

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime

  @belongsTo(()=>Field)
  public field: BelongsTo<typeof Field>

  @belongsTo(()=>User)
  public bookers: BelongsTo<typeof User>

  @manyToMany(()=>User, {
    pivotTable: 'schedules'
  })
  public players: ManyToMany<typeof User>
}
