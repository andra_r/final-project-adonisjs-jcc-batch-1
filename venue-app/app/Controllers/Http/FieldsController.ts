import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import Database from '@ioc:Adonis/Lucid/Database'
import FieldFormValidator from 'App/Validators/FieldFormValidator'
import Field from 'App/Models/Field'
import Venue from 'App/Models/Venue'

export default class FieldsController {
  public async index({response, request}:HttpContextContract){
    if (request.qs().name){
        let name = request.qs().name
        let fieldFiltered = await Field.findBy('name', name)
        return response.status(200).json({message: 'success', data: fieldFiltered})
    }
    let fields = await Field.all()
    return response.status(200).json({message: 'success', data: fields})
  }

  public async store ({request, response, params}: HttpContextContract) {
    const venue = await Venue.findByOrFail('id', params.venue_id)
    const newField = new Field()
    newField.name = request.input('name')
    newField.type = request.input('type')
    await newField.related('venue').associate(venue)
    return response.created({status: 'success', data: newField})
  }

  public async show ({response, params}: HttpContextContract) {
    let venue = await Field.query().where('id', params.id).preload('venue', (venueQuery)=>{venueQuery.select(['name', 'address', 'phone'])}).preload('bookings', (bookingQuery)=>{bookingQuery.select(['id', 'field_id', 'title', 'play_date_start', 'play_date_end', 'user_id'])}).firstOrFail()
        return response.ok({message: 'success', data: venue})
  }

  public async update ({params, request, response}: HttpContextContract) {
    let id = params.id
        let fieldUpdate = await Field.findOrFail(id)
        fieldUpdate.name = request.input('name')
        fieldUpdate.type = request.input('address')
        fieldUpdate.venueId = request.input('phone')
        fieldUpdate.save()
        return response.ok({message: 'updated!'})
  }

  public async destroy ({params, response}: HttpContextContract) {
    let venue = await Field.findOrFail(params.id)
        await venue.delete()
        return response.ok({message: 'deleted!'})
  }
}
