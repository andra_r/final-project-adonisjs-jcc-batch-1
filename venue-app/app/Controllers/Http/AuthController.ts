import { HttpContextContract } from '@ioc:Adonis/Core/HttpContext'
import User from 'App/Models/User'
import UserValidator from 'App/Validators/UserValidator'
import { schema } from '@ioc:Adonis/Core/Validator'
import Mail from '@ioc:Adonis/Addons/Mail'
import Database from '@ioc:Adonis/Lucid/Database'

export default class AuthController {

    /**
     * @swagger
     * /api/v1/register:
     *  post:
     *      tags:
     *          - Authentication
     *      summary: Register
     *      description: Register new user
     *      requestBody:
     *          required: true
     *          content:
     *              application/x-www-form-urlencoded:
     *                  schema:
     *                      $ref: '#definitions/User'
     *              application/json:
     *                  schema:
     *                      $ref: '#definitions/User'
     *      responses:
     *          201:
     *              description: user created, verify otp in email
     *          422:
     *              description: request invalid
     * 
     */
    public async register ({request, response}:HttpContextContract){
        
            const data = await request.validate(UserValidator)
            const name = request.input('name')
            const email = request.input('email')
            const password = request.input('password')
            const role = request.input('role')
            const newUser = await User.create({name, email, password, role})
            const otp_code = Math.floor(100000 + Math.random() * 900000)
            let saveCode = await Database.table('otp_codes').insert({otp_code: otp_code, user_id: newUser.id})
            await Mail.send((message) => {
                message
                  .from('admin@venue.com')
                  .to(email)
                  .subject('Welcome Onboard!')
                  .htmlView('emails/otp_verification', { otp_code })
              })          
           
            return response.created({message: 'register success, please verify!', data: newUser})
    }

    public async login ({auth, request, response}:HttpContextContract){
        const loginSchema = schema.create({
            email: schema.string({trim: true}),
            password: schema.string({trim: true})
        })
        const payload = await request.validate({schema: loginSchema})
        const token = await auth.use('api').attempt(payload.email, payload.password)
        return response.ok({status: 'login success', data: token})
    }

    public async otpVerify({request, response}:HttpContextContract){
        let otp_code = request.input('otp_code')
        let email = request.input('email')
        let user = await User.findBy('email', email)
        let otpCheck = await Database.query().from('otp_codes').where('otp_code', otp_code).first()

        if (user?.id == otpCheck.user_id){
            user!.isVerified = true
            await user?.save()
            return response.status(200).json({message: 'verify success!'})
        }
        else {
            return response.status(400).json({message: 'verify failed!'})
        }
    }
}
